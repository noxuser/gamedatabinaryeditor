
// binFileViewerDlg.h : 헤더 파일
//

#pragma once

#include "textLabel.h"
#include <memory>
#include <string>

class CoreUnit;

// CbinFileViewerDlg 대화 상자
class CbinFileViewerDlg : public CDialogEx
{
private:
	CMFCButton m_openButton;
	CMFCButton m_saveButton;
	CMFCButton m_gamedataSelect;
	CMFCButton m_monsterSelect;
	CMFCButton m_modifierSelect;
	CMFCButton m_soundsetSelect;
	
	CMFCButton m_eraseButton;
	CRichEditCtrl m_editor;
	CSliderCtrl m_verticalSlider;

	TextLabel m_topLabel;
	TextLabel m_searchInfoLabel;
	CEdit m_findEdit;
	CMFCButton m_findButton;

	std::unique_ptr<CoreUnit> m_core;
	
// 생성입니다.
public:
	CbinFileViewerDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.
	virtual ~CbinFileViewerDlg() override;

// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_BINFILEVIEWER_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.

private:
	void ComponentLoadCompleted();

	void UpdateEditor();
	void EmptyEditor();

// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg void OnClickLoadData();
	afx_msg void OnClickSaveData();
	afx_msg void OnClickErase();
	afx_msg void OnClickSubitem(UINT nId);
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd *pWnd, UINT nCtlColor);
	afx_msg void OnClickSearch();
	DECLARE_MESSAGE_MAP()

public:
	void Listener(std::string &message);
	static void MessageCb(void *ptr, std::string message);
};
