
#ifndef CORE_UNIT_H__
#define CORE_UNIT_H__

#include <memory>
#include <functional>
#include <map>

enum class BinFileTypes
{
	Gamedata = 0,
	Modifier = 1,
	Monster = 2,
	Soundset = 3
};

enum class MessageID
{
	MSG_NOTHING,
	MSG_CONNECT_OK,
	MSG_INVALID_KEY,
	MSG_LOAD_OK,
	MSG_CONTENT_NULL,
	MSG_WRITE_OK,
	MSG_WRITE_NG,
	MSG_WRITE_CTX_NULL,
	MSG_CLEAR_CTX
};

class ExternalLib;
class CDialogEx;

class CoreUnit
{
private:
	std::unique_ptr<ExternalLib> m_libModule;
	CString m_loadedFilename;
	std::map<std::string, MessageID> m_msgChecker;

public:
	explicit CoreUnit();
	virtual ~CoreUnit();

	void LibModuleInit(CDialogEx *cWnd, std::function<void(void*, std::string)> messageCb);

	CString LoadContent();
	void Open(const CString &filename, BinFileTypes type);
	void Save(const CString &content);
	void Empty();
	CString CurrentFilename() const;

private:
	void InitKeyChecker();

public:
	MessageID PickMessageID(std::string &message);
	
};

#endif