
#include "cryptKey.h"

CryptKey::cryptMod::cryptMod(const uint8_t _index, CryptKey::CryptMode _mode)
	: m_mapped(&cryptKeyV), m_mode(_mode), m_index(_index % 32), m_blowCtx(new blowfish_ctx())
{
	KeyGenerate();
	blowfish_set_key(m_blowCtx.get(), m_key.size(), &(m_key[0]));
}

void CryptKey::cryptMod::KeyGenerate()
{
	uint32_t index = m_index * 28;

	if (m_key.size())
		m_key.clear();
	m_key.resize(56);
	std::transform(&(*m_mapped)[index], &(*m_mapped)[index + 56], m_key.begin(), [](uint8_t chunk)->uint8_t { return chunk; });
}

bool CryptKey::cryptMod::StreamCrypt(std::vector<uint8_t> &srcV, std::vector<uint8_t> &destV)
{
	bool chkDev8 = srcV.size() != 0 ? (srcV.size() % 8) == 0 : false;

	if (chkDev8)
	{
		destV.resize(srcV.size());
		switch (m_mode)
		{
		case CryptKey::CryptMode::NOTHING:
			break;
		case CryptKey::CryptMode::DECRYPT:
			blowfish_decrypt(m_blowCtx.get(), destV.size(), &destV[0], &srcV[0]);
			break;
		case CryptKey::CryptMode::ENCRYPT:
			blowfish_encrypt(m_blowCtx.get(), destV.size(), &destV[0], &srcV[0]);
			break;
		default:
			return false;
		}
		return true;
	}
	return false;
}