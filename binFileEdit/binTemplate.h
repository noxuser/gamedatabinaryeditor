
#ifndef BIN_TEMPLATE_H__
#define BIN_TEMPLATE_H__

#include "utils\fileStream.h"
#include "keyTypes.h"
#include <vector>
#include <string>
#include <atlstr.h>

class BinTemplate : public FileStream
{
private:
	const KeyTypesArea::KeyTypes m_key;
	std::vector<uint8_t> m_streamOutV;
	std::vector<uint8_t> m_streamV;
	bool m_readComplete;

public:
	explicit BinTemplate(const std::string &filename = {}, KeyTypesArea::KeyTypes keyType = KeyTypesArea::KeyTypes::Gamedata);
	virtual ~BinTemplate() override;

private:
	bool CheckBinaryHeader(std::vector<uint8_t> *fstream);
	virtual void ReportReadComplete(std::vector<uint8_t> *fstream) override;

	virtual void ReportWriteReady(std::vector<uint8_t> *fstream) override;

public:
	bool CheckLoadCompleted() const;
	CString GetStream();
	void SetStream(const std::string &wrStream);
};

#endif