
#ifndef KEY_TYPES_H__
#define KEY_TYPES_H__

#include <vector>

namespace KeyTypesArea
{
	enum class KeyTypes
	{
		Thema = 1,
		Soundset = 5,
		ThingDb = 7,
		Gamedata = 8,
		Modifier = 13,
		MapFormat = 19,
		Monster = 23,
		PlrFormat = 27
	};

	constexpr uint32_t headerLength = 8;

	const std::vector<uint8_t> gameDbHeader({ 0x27, 0x65, 0x4A, 0x52, 0x1B, 0xC5, 0xE0, 0xC1 });
	const std::vector<uint8_t> soundsetHeader({ 0xCD, 0x1F, 0x6D, 0xA0, 0x31, 0xF8, 0x18, 0x6C });
	const std::vector<uint8_t> modifierHeader({ 0x2E, 0x3D, 0x8F, 0xEB, 0xE3, 0xAD, 0x27, 0xB2 });
	const std::vector<uint8_t> monsterHeader({ 0x41, 0xBE, 0xD1, 0xA6, 0xCB, 0x05, 0x16, 0xB0 });

	template <typename container>
	bool compareAt(typename container::const_iterator _first, typename container::const_iterator _last, typename container::const_iterator _targetFirst)
	{
		while (_first != _last)
		{
			if ((*_first) != (*_targetFirst))
				return false;
			++_first;
			++_targetFirst;
		}
		return true;
	}

}

#endif