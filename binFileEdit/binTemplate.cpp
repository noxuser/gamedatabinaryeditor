
#include "binTemplate.h"
#include "blowfish\cryptKey.h"
#include <iterator>

BinTemplate::BinTemplate(const std::string &filename, KeyTypesArea::KeyTypes keyType)
	: FileStream(filename), m_key(keyType)
{
	m_readComplete = false;
}

BinTemplate::~BinTemplate()
{ }


bool BinTemplate::CheckBinaryHeader(std::vector<uint8_t> *fstream)
{
	if (fstream->size() < KeyTypesArea::headerLength)
		return false;

	std::vector<uint8_t>::const_iterator headerIterator;

	switch (m_key)
	{
	case KeyTypesArea::KeyTypes::Gamedata:
		headerIterator = KeyTypesArea::gameDbHeader.cbegin();
		break;
	case KeyTypesArea::KeyTypes::Modifier:
		headerIterator = KeyTypesArea::modifierHeader.cbegin();
		break;
	case KeyTypesArea::KeyTypes::Monster:
		headerIterator = KeyTypesArea::monsterHeader.cbegin();
		break;
	case KeyTypesArea::KeyTypes::Soundset:
		headerIterator = KeyTypesArea::soundsetHeader.cbegin();
		break;
	default:
		return false;
	}

	return KeyTypesArea::compareAt<std::vector<uint8_t>>(fstream->cbegin(), fstream->cbegin()+8, headerIterator);
}

void BinTemplate::ReportReadComplete(std::vector<uint8_t> *fstream)
{
	if (CheckBinaryHeader(fstream))
	{
		CryptKey::cryptMod decrypt(static_cast<uint8_t>(m_key), CryptKey::CryptMode::DECRYPT);
		m_readComplete = decrypt.StreamCrypt(*fstream, m_streamV);
	}
}

void BinTemplate::ReportWriteReady(std::vector<uint8_t> *fstream)
{
	uint32_t requirePadding = 8 - (m_streamOutV.size() % 8);

	while (requirePadding--)
		m_streamOutV.push_back(0);
	CryptKey::cryptMod encrypt(static_cast<uint8_t>(m_key), CryptKey::CryptMode::ENCRYPT);

	encrypt.StreamCrypt(m_streamOutV, *fstream);
}

bool BinTemplate::CheckLoadCompleted() const
{
	return m_readComplete;
}

CString BinTemplate::GetStream()
{
	if (m_streamV.size())
	{
		m_streamV.push_back(0);
		return CString(m_streamV.data());
	}
	return{};
}

void BinTemplate::SetStream(const std::string &wrStream)
{
	if (wrStream.length())
	{
		m_streamOutV.resize(wrStream.size());
		std::transform(wrStream.begin(), wrStream.end(), m_streamOutV.begin(), [](uint8_t chk) { return chk; });
	}
}