
#define MAKEDLL
#include "binHandler.h"
#include "binTemplate.h"
#include "keyTypes.h"
#include "utils\stringUtils.h"

BinHandler::BinHandler()
{
	m_template = nullptr;
	m_messageCb = BinHandler::DefaultMessage;
	MakeParseKey();
}

BinHandler::~BinHandler()
{ }


void BinHandler::MakeParseKey()
{
	m_parseKey.emplace("Gamedata", static_cast<uint32_t>(KeyTypesArea::KeyTypes::Gamedata));
	m_parseKey.emplace("Modifier", static_cast<uint32_t>(KeyTypesArea::KeyTypes::Modifier));
	m_parseKey.emplace("Soundset", static_cast<uint32_t>(KeyTypesArea::KeyTypes::Soundset));
	m_parseKey.emplace("Monster", static_cast<uint32_t>(KeyTypesArea::KeyTypes::Monster));
}

uint32_t BinHandler::GetParseKey(const std::string &keyword)
{
	std::map<std::string, uint32_t>::const_iterator keyIterator = m_parseKey.find(keyword);

	if (keyIterator != m_parseKey.cend())
		return keyIterator->second;
	return -1;
}

void BinHandler::Open(const std::string &filename, const std::string &keyword)
{
	uint32_t getkey = GetParseKey(keyword);

	if (getkey >= 0)
	{
		m_template.reset(new BinTemplate(filename, static_cast<KeyTypesArea::KeyTypes>(getkey)));

		m_template->FileProcess();	//스트림 읽어왔다

		if (m_template->CheckLoadCompleted())
			m_messageCb(m_target, std::string("LoadComplete"));
		else
			m_messageCb(m_target, std::string("EmptyContent"));
	}
	else
		m_messageCb(m_target, std::string("InvalidKey"));
}

void BinHandler::Save(const CString &outStream)
{
	if (m_template != nullptr)
	{
		if (!m_template->CheckLoadCompleted())
			return;
		std::string destStream;
		std::wstring srcStream(outStream.operator LPCWSTR());

		StringUtils::UnicodeToAnsi(srcStream, destStream);

		m_template->ChangeFileMode(FileMode::FILE_MODE_WRITE);
		m_template->SetStream(destStream);
		if (m_template->FileProcess())
			m_messageCb(m_target, std::string("WriteOK"));
		else
			m_messageCb(m_target, std::string("WriteNG"));
	}
	else
		m_messageCb(m_target, std::string("WriteContextNull"));
}

void BinHandler::Clear()
{
	if (m_template != nullptr)
	{
		m_template.reset(nullptr);
		m_messageCb(m_target, std::string("ClearCtx"));
	}
}

CString BinHandler::ReadContent()
{
	if (m_template != nullptr)
		return m_template->GetStream();
	return{};
}

void BinHandler::DefaultMessage(MessageTarget ptr, std::string message)
{ }

void BinHandler::Connection(MessageTarget target, MessageCbType cb)
{
	m_target = target;
	m_messageCb = cb;

	m_messageCb(m_target, std::string("Connected"));
}
