
#ifndef BIN_HANDLER_H__
#define BIN_HANDLER_H__


#include "exportModule.h"
#include <vector>
#include <map>


class BinTemplate;

class BinHandler : public ExternalLib
{
private:
	std::unique_ptr<BinTemplate> m_template;
	std::map<std::string, uint32_t> m_parseKey;
	MessageCbType m_messageCb;
	MessageTarget m_target;

public:
	explicit BinHandler();
	virtual ~BinHandler() override;

private:
	void MakeParseKey();
	uint32_t GetParseKey(const std::string &keyword);

public:
	virtual void Open(const std::string &filename, const std::string &keyword) override;
	virtual void Save(const CString &outStream) override;
	virtual void Clear() override;
	virtual CString ReadContent() override;

private:
	static void DefaultMessage(MessageTarget ptr, std::string message);
	virtual void Connection(MessageTarget target, MessageCbType cb) override;
};

#endif